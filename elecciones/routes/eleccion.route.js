var express = require('express')
var eleccion = express.Router();
var controller = require('../controllers/eleccion.controller');

eleccion.get('/eleccion/get_all', controller.default.getInstance().get_all);
eleccion.post('/eleccion/create', controller.default.getInstance().create);
module.exports = eleccion;