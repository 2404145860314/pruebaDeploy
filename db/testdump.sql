create table Departamento(
	id_departamento integer not null auto_increment primary key,
	nombre varchar(100)
);

create table Municipio(
	id_municipio integer not null auto_increment primary key,
    nombre varchar(100),
    id_departamento integer,
	foreign key (id_departamento) references Departamento(id_departamento)
);

create table Persona(
	cui integer primary key,
	nombre varchar(80),
    apellido varchar(80),
    telefono varchar(15),
    foto varchar(220),
    pin integer,
    correo varchar(100),
    mac varchar(50),
    tipo varchar(100),
    pais varchar(30),
    estado varchar(50),
    id_municipio integer,
    foreign key (id_municipio) references Municipio(id_municipio)
);

create table Eleccion(
	id_eleccion integer not null auto_increment primary key,
    titulo varchar(120),
    descripcion varchar(300),
    fecha_ini datetime,
    fecha_fin datetime,
    ubicacion varchar(100)
);

create table Candidato(
	cui integer primary key,
    Partido varchar(100),
    foto varchar(300),
    imagen_partido varchar(300),
    departamento varchar(35),
    municipio varchar(35), 
    id_eleccion integer,
    foreign key (id_eleccion) references Eleccion(id_eleccion)
);

create table Asignar_eleccion(
	id_asignElec integer not null auto_increment primary key,
    fecha datetime,
    id_persona integer,
    id_eleccion integer,
    foreign key (id_persona) references Persona(cui),
    foreign key (id_eleccion) references Eleccion(id_eleccion)
);

create table Votacion(
	id_votacion integer not null auto_increment primary key,
	fecha datetime,
    estado varchar(100),
    aprobado integer,
    ubicacion varchar(100),
    id_candidato integer,
    id_persona integer,
    foreign key (id_candidato) references Candidato(cui),
    foreign key (id_persona) references Persona(cui)
);


create table Registro(
	cui integer primary key,
    nombre varchar(100),
    expediente integer
);

insert into Departamento(nombre) values('Alta Verapaz');
insert into Departamento(nombre) values('Baja Verapaz');
insert into Departamento(nombre) values('Chimaltenago');
insert into Departamento(nombre) values('Chiquimula');
insert into Departamento(nombre) values('Guatemala');
insert into Departamento(nombre) values('El Progreso');
insert into Departamento(nombre) values('Escuintla');
insert into Departamento(nombre) values('Huehuetenango');
insert into Departamento(nombre) values('Izabal');
insert into Departamento(nombre) values('Jalapa');
insert into Departamento(nombre) values('Jutiapa');
insert into Departamento(nombre) values('Petén');
insert into Departamento(nombre) values('Quetzaltenango');
insert into Departamento(nombre) values('Quiché');
insert into Departamento(nombre) values('Retalhuleu');
insert into Departamento(nombre) values('Sacatepequez');
insert into Departamento(nombre) values('San Marcos');
insert into Departamento(nombre) values('Santa Rosa');
insert into Departamento(nombre) values('Sololá');
insert into Departamento(nombre) values('Suchitepequez');
insert into Departamento(nombre) values('Totonicapán');
insert into Departamento(nombre) values('Zacapa');

insert into Municipio values(1,'Villa Nueva',5);
insert into Persona values(1234,'Hil','Perucho','12345678','foto.jpg',123,'hil@gmail.com','ABCD','User','Villa Nueva','A',1);


ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'password';

flush privileges;
