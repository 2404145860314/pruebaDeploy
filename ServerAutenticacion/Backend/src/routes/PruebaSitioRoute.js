var express = require('express')
var prueba = express.Router();
var controller = require('../controllers/PruebaSitioController');

prueba.get('/prueba/task', controller.default.getInstance().task);
prueba.get('/prueba/private', controller.default.getInstance().taskPrivate);
module.exports = prueba;