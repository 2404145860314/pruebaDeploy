var database = require("../config/database");
const jwt = require("jsonwebtoken");

var PruebaSitio = (function () {
    function PruebaSitio() {
        this.task = function (req, res){
            res.json([{
                id: 1,
                name:'Task 1'
            },
            {
                id: 2,
                name:'Task 2'
            }
            ]);
        };

        this.taskPrivate = function (req, res){
            res.json([{
                id: 1,
                name:'Task 1'
            },
            {
                id: 2,
                name:'Task 2'
            }
            ]);
        };
    }
    PruebaSitio.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    return PruebaSitio;
}());

exports.default = PruebaSitio;