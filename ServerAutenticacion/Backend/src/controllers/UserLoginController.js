var database = require("../config/database");
const jwt = require("jsonwebtoken");
const { getConsoleOutput } = require("@jest/console");

var UserLoginController = (function () {
    function UserLoginController() {
        /* Realizar Login*/
        this.get_login = function (req, res) {

            var query = "SELECT * FROM Persona WHERE cui = ? and correo = ?;"
            var body = {
                cui: req.body.cui,
                correo: req.body.correo
            }
            
            database.query(query, [body.cui,body.correo], function (err, data) {
                //console.log(data.lenght);
                //console.log(data)
                if (err) {
                    res.status(400).json({
                        estado: false,
                        status: 400,
                        error: err
                    });
                } else {
                    if (data[0] == undefined ){
                        res.status(400).json({
                            estado: false,
                            status: 400,
                            error: 'Inconveniente al iniciar sesion'
                        });
                    } else {
                        const token = jwt.sign(body.correo, 'savacacionessisale');
                        res.json({
                            estado: true,
                            status: 200,
                            mensaje: "Inicio de Sesion Correctamente",
                            json: token
                        });
                    }
                    
                }
                
                
            });
        }; 
    }
    UserLoginController.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    return UserLoginController;
}());

exports.default = UserLoginController;