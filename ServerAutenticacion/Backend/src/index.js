const express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

const app = express();



//Body-Parser Config
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ limit: '50mb' }));
app.use(cors())

//Headers
app.use(function(req, res, next) {
    
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    if (req.methods == "OPTIONS") {
        res.sendStatus(200);
    } else {
        next();
    }
});

var login = require('./routes/UserLoginRoute');
var prueba = require('./routes/PruebaSitioRoute');
const verifyToken = require('./routes/validarTokenRoute');

app.use("/", login);
app.use("/", verifyToken, prueba);


app.set('port', process.env.PORT || 3000)
module.exports = app;