#!/bin/bash
set -eo pipefail
echo "Subiendo mis imagenes a gitlab XD!!"
filename="version"
while read -r line; do
    echo "$line"
    docker tag resultados registry.gitlab.com/hilbert.perucho/sa_proyecto_g11:resultados${line}
    docker tag elecciones registry.gitlab.com/hilbert.perucho/sa_proyecto_g11:elecciones${line}
    
    docker push registry.gitlab.com/hilbert.perucho/sa_proyecto_g11:resultados${line}
    docker push registry.gitlab.com/hilbert.perucho/sa_proyecto_g11:elecciones${line}

    echo "fin del script xd"
done < "$filename"