create table Departamento(
	id_departamento integer not null auto_increment primary key,
	nombre varchar(100)
);

create table Municipio(
	id_municipio integer not null auto_increment primary key,
    nombre varchar(100),
    id_departamento integer,
	foreign key (id_departamento) references Departamento(id_departamento)
);

create table Persona(
	cui integer primary key,
	nombre varchar(80),
    apellido varchar(80),
    telefono varchar(15),
    foto varchar(220),
    pin integer,
    correo varchar(100),
    mac varchar(50),
    tipo varchar(100),
    pais varchar(30),
    estado varchar(50),
    id_municipio integer,
    foreign key (id_municipio) references Municipio(id_municipio)
);
select * from persona;
create table Eleccion(
	id_eleccion integer not null auto_increment primary key,
    titulo varchar(120),
    descripcion varchar(300),
    fecha_ini datetime,
    fecha_fin datetime,
    ubicacion varchar(100)
);

insert into Eleccion (titulo, descripcion, fecha_ini, fecha_fin, ubicacion) values('Presidente', 'Se debatira la seleccion de un presidente', '2022-06-20 00:00:00', '2022-06-28 00:00:00', 'Capital Guatemala');
select * from Eleccion;

create table Candidato(
	cui integer primary key,
    Partido varchar(100),
    foto varchar(300),
    imagen_partido varchar(300),
    departamento varchar(35),
    municipio varchar(35), 
    id_eleccion integer,
    foreign key (id_eleccion) references Eleccion(id_eleccion)
);

insert into Candidato values(909, 'partido1', 'FotoCandidato', 'IMGPartido', 'Alta Verapaz', 'Coban', 1);
insert into Candidato values(808, 'partido2', 'Candidato', 'PartidoIMG', 'Baja Verapaz', 'Salama', 1);
select * from Candidato;

create table Asignar_eleccion(
	id_asignElec integer not null auto_increment primary key,
    fecha datetime,
    id_persona integer,
    id_eleccion integer,
    foreign key (id_persona) references Persona(cui),
    foreign key (id_eleccion) references Eleccion(id_eleccion)
);

select * from Asignar_eleccion;
truncate table Asignar_eleccion;

create table Votacion(
	id_votacion integer not null auto_increment primary key,
    estado varchar(100),
    aprobado integer,
    ubicacion varchar(100),
    id_candidato integer,
    id_persona integer,
    foreign key (id_candidato) references Candidato(cui),
    foreign key (id_persona) references Persona(cui)
);


create table Registro(
	cui integer primary key,
    nombre varchar(100),
    expediente integer
);

insert into Departamento (nombre) values('Guatemala');
select * from Municipio;
insert into Municipio (nombre, id_departamento) values('Guatemala', 1);
insert into Municipio (nombre, id_departamento) values('Mixco', 1);

insert into Candidato values(909, 'partido1', 'FotoCandidato', 'IMGPartido', 'Alta Verapaz', 'Coban');
insert into Candidato values(808, 'partido2', 'Candidato', 'PartidoIMG', 'Baja Verapaz', 'Salama');

insert into Persona values(111, 'diego', 'vasquez', '12345678', 'FotoURL', 546, 'correo@mail.com', 'soltero', 'Guatemala', '', 1);