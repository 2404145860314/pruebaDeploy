const server = require("../index")
const chai = require("chai")
const chaiHttp = require("chai-http")

chai.should()
chai.use(chaiHttp)

describe("Microservicio Diego: ", () => {

    describe("Probando Post /RegistrarPersona", () => {

        it("Error al ejecutar /RegistrarPersona", (done) => {
            chai.request(server)
            .post("/RegistrarPersona")
            .send({
                cui: 555,
                nombr: "Prueba",
                id_municipio: 1
            })
            .end((err, res) => {
                res.should.have.status(404);
                done()
            })
        });

        /*it("Contenido del JSON", (done) => {
            chai.request(server).post("/RegistrarPersona").send({
                cui: 555,
                nombre: "Prueba",
                apellido: "Test",
                telefono: "11112222",
                foto: "PruebaFoto",
                pin: "363",
                correo: "Prueba@Prueba.com.gt",
                mac: "TEST",
                tipo: "Probandp",
                pais: "Guatemala",
                estado: "",
                id_municipio: 1
            }).end((err, res)=> {
                res.should.have.status(200);
                done()
            });
        })*/
    });
})